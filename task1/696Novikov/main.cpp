#include <GL/glew.h>
#include <GLFW/glfw3.h>

// #define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <SOIL2.h>

#include <chrono>
#include <thread>
// #include <experimental/filesystem>
// #include <iomanip>
#include <iostream>
#include <fstream>
#include <vector>
#include <fstream>

#define DATA_DIR "./696NovikovData1"
#define IMG_WIDTH 1024
#define IMG_HEIGHT 1024
#define WG_WIDTH 16
#define WG_HEIGHT 16
#define N_SPHERES 3

struct Sphere {
    GLfloat x, y, z, rad;
    unsigned int r, g, b;
    char padding[4 + 32 + 64 + 128];
}  spheres[N_SPHERES] = {
    { 2.0, 5.0,  2.0, 1.0, 255,   0, 255 },
    { 2.0,  5.0, 1.0, 1.0,   0, 255,   0 },
    { 0.0,  5.0, 0.0, 1.0, 255,   0,   0 },
};

namespace clock_utils
{

using namespace std::chrono;

class FPSLimiter
{
    duration<double> m_sleep;
    bool is_started;
    time_point<steady_clock> last;
public:
    FPSLimiter(double sleep_s) :
        m_sleep(sleep_s),
        is_started(false),
        last() {}
    
    void start()
    {
        is_started = true;
        last = steady_clock::now();
    }
    
    void wait()
    {
        if (not is_started)
            start();
        else
        {
            auto now = steady_clock::now();
            duration<double> dur = now - last;
            
            std::this_thread::sleep_for(max(duration<double>(0.0), m_sleep - dur));
            last = steady_clock::now();
        }
    };
    
    void stop()
    {
        is_started = false;
    }
};

class Measurer
{
    time_point<steady_clock> last;
public:
    Measurer() :
        last()
    {
        update();
    }
    
    void update()
    {
        last = steady_clock::now();
    }
    
    double get_s()
    {
        duration<double, std::ratio<1>> dur = steady_clock::now() - last;
        return dur.count();
    }
};

}
using clock_utils::FPSLimiter;
using clock_utils::Measurer;

Measurer global_measurer;

#define VELOCITY_COEF 4.0f
#define ANGLE_COEF 0.01f

bool move_keys_pressed[4];
bool mouse_button_pressed[2];
double xpos_start[2], ypos_start[2];
glm::mat4 pos_mat = glm::mat4(1.0);
glm::mat4 inv_rot_mat = glm::mat4(1.0);
glm::vec3 translate[] = {
    glm::vec3( 0.0, -1.0, 0.0),
    glm::vec3( 1.0,  0.0, 0.0),
    glm::vec3( 0.0,  1.0, 0.0),
    glm::vec3(-1.0,  0.0, 0.0),
};

/*
 * Controls
 * WASD - move forward/left/backward/right
 * Right mouse button + mouse move left/right - rotate left/right
 *
 */

void update_position(GLFWwindow *window)
{
    float t = static_cast<float>(global_measurer.get_s());
    
    glm::vec3 dv = glm::vec3(0.0, 0.0, 0.0);
    for (int i = 0; i < 4; ++i)
        if (move_keys_pressed[i])
            dv += translate[i];
    pos_mat = glm::translate(glm::mat4(1.0), dv * t * VELOCITY_COEF) * pos_mat;
    
    global_measurer.update();
    
    if (mouse_button_pressed[0])
    {
        double xpos = xpos_start[0], ypos = ypos_start[0];
        glfwGetCursorPos(window, &xpos, &ypos);
        float dphi = ANGLE_COEF * (xpos - xpos_start[0]);
        pos_mat = glm::rotate(glm::mat4(1.0), dphi, glm::vec3(0.0f, 0.0f, 1.0f)) * pos_mat;
        inv_rot_mat = inv_rot_mat * glm::rotate(glm::mat4(1.0), -dphi, glm::vec3(0.0f, 0.0f, 1.0f));
        xpos_start[0] = xpos;
        ypos_start[0] = ypos;
    }
}

void mouseButtonCallback(GLFWwindow *window, int button, int action, int mods)
{
    if (button == GLFW_MOUSE_BUTTON_1)
    {
        mouse_button_pressed[0] = (action == GLFW_PRESS) ? 1 : 0;
        glfwGetCursorPos(window, &xpos_start[0], &ypos_start[0]);
    }
    else if (button == GLFW_MOUSE_BUTTON_2)
    {
        mouse_button_pressed[1] = (action == GLFW_PRESS) ? 1 : 0;
        glfwGetCursorPos(window, &xpos_start[1], &ypos_start[1]);
    }
}

void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    switch (key)
    {
        case GLFW_KEY_ESCAPE: {
            glfwSetWindowShouldClose(window, GL_TRUE);
            break;
        }
        case GLFW_KEY_W: {
            if (action == GLFW_PRESS)
                move_keys_pressed[0] = true;
            else if (action == GLFW_RELEASE)
                move_keys_pressed[0] = false;
            break;
        }
        case GLFW_KEY_A: {
            if (action == GLFW_PRESS)
                move_keys_pressed[1] = true;
            else if (action == GLFW_RELEASE)
                move_keys_pressed[1] = false;
            break;
        }
        case GLFW_KEY_S: {
            if (action == GLFW_PRESS)
                move_keys_pressed[2] = true;
            else if (action == GLFW_RELEASE)
                move_keys_pressed[2] = false;
            break;
        }
        case GLFW_KEY_D: {
            if (action == GLFW_PRESS)
                move_keys_pressed[3] = true;
            else if (action == GLFW_RELEASE)
                move_keys_pressed[3] = false;
            break;
        }
    }
}

// https://github.com/fendevel/Guide-to-Modern-OpenGL-Functions#detailed-messages-with-debug-output
void debugCallbackOpengl(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, GLchar const* message, void const*)
{
    auto const src_str = [source]() {
        switch (source)
        {
        case GL_DEBUG_SOURCE_API: return "API";
        case GL_DEBUG_SOURCE_WINDOW_SYSTEM: return "WINDOW SYSTEM";
        case GL_DEBUG_SOURCE_SHADER_COMPILER: return "SHADER COMPILER";
        case GL_DEBUG_SOURCE_THIRD_PARTY: return "THIRD PARTY";
        case GL_DEBUG_SOURCE_APPLICATION: return "APPLICATION";
        case GL_DEBUG_SOURCE_OTHER: return "OTHER";
        }
        return "???";
    }();

    auto const type_str = [type]() {
        switch (type)
        {
        case GL_DEBUG_TYPE_ERROR: return "ERROR";
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: return "DEPRECATED_BEHAVIOR";
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR: return "UNDEFINED_BEHAVIOR";
        case GL_DEBUG_TYPE_PORTABILITY: return "PORTABILITY";
        case GL_DEBUG_TYPE_PERFORMANCE: return "PERFORMANCE";
        case GL_DEBUG_TYPE_MARKER: return "MARKER";
        case GL_DEBUG_TYPE_OTHER: return "OTHER";
        }
        return "???";
    }();

    if (severity == GL_DEBUG_SEVERITY_NOTIFICATION)
        return;

    auto const severity_str = [severity]() {
        switch (severity) {
        case GL_DEBUG_SEVERITY_NOTIFICATION: return "NOTIFICATION";
        case GL_DEBUG_SEVERITY_LOW: return "LOW";
        case GL_DEBUG_SEVERITY_MEDIUM: return "MEDIUM";
        case GL_DEBUG_SEVERITY_HIGH: return "HIGH";
        }
        return "???";
    }();

    std::cout << src_str << ", " << type_str << ", " << severity_str << ", " << id << ": " << message << '\n';
}

char *readFile(const char *filename)
{
    std::ifstream fin(filename);
    int c;
    std::string res;
    while ((c = fin.get()) != -1)
        res.push_back(static_cast<char>(c));
    char *data = new char[res.size() + 1];
    strcpy(data, res.c_str());
    return data;
}

namespace opengl_utils
{

GLuint compileShaderFile(const char *filename, GLenum shader_type)
{
    char *shader_text = readFile(filename);
    GLuint shader = glCreateShader(shader_type);
    glShaderSource(shader, 1, &shader_text, nullptr);
    delete [] shader_text;
    
    glCompileShader(shader);
    
    GLint status = -1;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
    if (status != GL_TRUE)
    {
        GLint errorLength;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &errorLength);
        
        std::string errorMessage(errorLength, ' ');
        errorMessage.resize(errorLength);

        glGetShaderInfoLog(shader, errorLength, 0, &errorMessage[0]);

        std::cerr << "Failed to compile the shader " << filename
                << "\nError message:\n" << errorMessage.data() << std::endl;
        
        exit(1);
    }
    
    return shader;
}

GLuint makeShaderProgram(const std::vector<GLuint> &shaders)
{
    GLuint program = glCreateProgram();
    
    /* shaders are supposed to be already built */
    for (const GLuint &shader : shaders)
        glAttachShader(program, shader);
    
    
    glLinkProgram(program);
    
    GLint status = -1;
    glGetProgramiv(program, GL_LINK_STATUS, &status);
    if (status != GL_TRUE)
    {
        GLint errorLength;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &errorLength);

        std::vector<char> errorMessage;
        errorMessage.resize(errorLength);

        glGetProgramInfoLog(program, errorLength, 0, errorMessage.data());

        std::cerr << "Failed to link a program:\n" << errorMessage.data() << std::endl;

        exit(1);
    }
    return program;
}

}
using opengl_utils::compileShaderFile;
using opengl_utils::makeShaderProgram;

int main()
{
    /* Initialize glfw and glew */
    if (!glfwInit())
    {
        std::cerr << "ERROR: could not start GLFW3\n";
        exit(1);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    GLFWwindow* window = glfwCreateWindow(600, 600, "MIPT OpenGL demos", nullptr, nullptr);
    if (!window)
    {
        std::cerr << "ERROR: could not open window with GLFW3\n";
        glfwTerminate();
        exit(1);
    }

    glfwMakeContextCurrent(window);
    glfwSetKeyCallback(window, keyCallback);
    glfwSetMouseButtonCallback(window, mouseButtonCallback);

    glewExperimental = GL_TRUE;
    glewInit();
    
    glDebugMessageCallback(debugCallbackOpengl, nullptr);
    glEnable(GL_DEBUG_OUTPUT);
    
    const GLubyte* renderer = glGetString(GL_RENDERER);
    const GLubyte* version = glGetString(GL_VERSION);
    const GLubyte* glslversion = glGetString(GL_SHADING_LANGUAGE_VERSION);
    std::cout << "Renderer: " << renderer << std::endl;
    std::cout << "OpenGL context version: " << version << std::endl;
    std::cout << "GLSL version: " << glslversion << std::endl;
    
    int max_compute_wg_size = 0,
        max_compute_wg_inv = 0,
        max_compute_wg_count = 0;
    glGetIntegerv(GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS, &max_compute_wg_inv);
    int size[3], cnt[3];
    for (int i = 0; i < 3; ++i)
    {
        glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, i, &size[i]);
        glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, i, &cnt[i]);
    }
    
    std::cout << "Compute shader max work group size: " << size[0] << ' '
                << size[1] << ' ' << size[2] << std::endl;
    std::cout << "Compute shader max work group invocations: " << max_compute_wg_inv << std::endl;
    std::cout << "Compute shader max work group count: " << cnt[0] << ' '
                << cnt[1] << ' ' << cnt[2] << std::endl;
    
    /* Create program and load shaders */
    GLuint vert_shader = compileShaderFile(DATA_DIR "/vert-shader.glsl", GL_VERTEX_SHADER);
    GLuint frag_shader = compileShaderFile(DATA_DIR "/frag-shader.glsl", GL_FRAGMENT_SHADER);
    
    GLuint program = makeShaderProgram({vert_shader, frag_shader});
    
    /* Initialize program for compute shader */
    GLuint comp_shader = compileShaderFile(DATA_DIR "/comp-shader.glsl", GL_COMPUTE_SHADER);
    
    GLuint comp_program = makeShaderProgram({comp_shader});
    
    /* Initialize data */
    /*   initialize vbo & vao */
    GLfloat vertices_buffer[] = {
        -1.0, -1.0,
        -1.0,  1.0,
         1.0,  1.0,
         1.0, -1.0,
    };
    GLuint vbo, vao;
    glCreateBuffers(1, &vbo);
    glNamedBufferData(vbo, 8 * sizeof(vertices_buffer[0]), vertices_buffer, GL_STATIC_DRAW);
    
    glCreateVertexArrays(1, &vao);
    /* (vaobj, bindingindex, buffer, offset, stride) */
    glVertexArrayVertexBuffer(vao, 0, vbo, 0, sizeof(vertices_buffer[0]) * 2);
    
    /* (vaobj, attribindex) */
    glEnableVertexArrayAttrib(vao, 0);
    /* (vaobj, attribindex, size, type, normalized, relativeoffset) */
    glVertexArrayAttribFormat(vao, 0, 2, GL_FLOAT, GL_FALSE, 0);
    /* (vaobj, attribindex, bindingindex) */
    glVertexArrayAttribBinding(vao, 0, 0);
    
    /*   initialize texture */
    GLuint tex;
    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);
    // glCreateTextures(GL_TEXTURE_2D, 1, &tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    // glTextureParameteri(tex, GL_TEXTURE_WRAP_S, GL_CLAMP);
    // glTextureParameteri(tex, GL_TEXTURE_WRAP_T, GL_CLAMP);
    // glTextureParameteri(tex, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    // glTextureParameteri(tex, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    
    int tex_w = IMG_WIDTH, tex_h = IMG_HEIGHT;
    GLubyte *pixels = new GLubyte[tex_w * tex_h * 4];
    for (int i = 0; i < tex_w; ++i)
        for (int j = 0; j < tex_h; ++j)
        {
            int p = j * 4 + i * 4 * tex_h;
            pixels[p + 0] = i;
            pixels[p + 1] = j;
            pixels[p + 2] = i + j;
            pixels[p + 3] = 255;
        }
    
    //glTextureStorage2D(tex, 1, GL_RGBA8, tex_w, tex_h);
    //glTextureSubImage2D(tex, 0, 0, 0, tex_w, tex_h, GL_RGBA,  GL_UNSIGNED_INT_8_8_8_8_REV, pixels);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, tex_w, tex_h, 0, 
            GL_RGBA, GL_UNSIGNED_INT_8_8_8_8_REV, pixels);
    glGenerateMipmap(GL_TEXTURE_2D);

    GLuint sampler_uniform = glGetUniformLocation(program, "texSampler");
    
    /*   run compute shader */
    int offset_alignment = -1;
    glGetIntegerv(GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT, &offset_alignment);
    std::cout << "Offset alignment: " << offset_alignment << std::endl;
    int max_bindings = -1;
    glGetIntegerv(GL_MAX_UNIFORM_BUFFER_BINDINGS, &max_bindings);
    std::cout << "Max uniform bindings: " << max_bindings << std::endl;
    
    static_assert(sizeof(Sphere) == 256);
    
    GLuint bo_comp;
    glCreateBuffers(1, &bo_comp);
    glNamedBufferData(bo_comp, N_SPHERES * sizeof(Sphere), spheres, GL_STATIC_READ);
    
    GLuint spheres_uniform = glGetUniformBlockIndex(comp_program, "Spheres");
    /* (program, uniformBlockIndex, blockBinding) */
    glUniformBlockBinding(comp_program, spheres_uniform, 0);
    std::vector<GLuint> buffers(N_SPHERES, bo_comp);
    std::vector<GLintptr> offsets(N_SPHERES, 0);
    for (int i = 1; i < N_SPHERES; ++i)
        offsets[i] = offsets[i - 1] + sizeof(Sphere);
    std::vector<GLintptr> sizes(N_SPHERES, sizeof(Sphere));
    
    GLint width_uniform = glGetUniformLocation(comp_program, "img_width");
    GLint height_uniform = glGetUniformLocation(comp_program, "img_height");
    
    GLint pos_mat_uniform = glGetUniformLocation(comp_program, "position_mat");
    GLint inv_rot_mat_uniform = glGetUniformLocation(comp_program, "inv_rot_mat");
    
    glBindBuffersRange(GL_UNIFORM_BUFFER, 0, N_SPHERES, buffers.data(),
                        offsets.data(), sizes.data());
    
    GLuint tex_2d = SOIL_load_OGL_texture(
            DATA_DIR "/2k_earth_daymap.jpg", 
            SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID,
            SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | 
            SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT);
    GLint texture1_uniform = glGetUniformLocation(comp_program, "texture1");
    
    if (tex_2d == 0)
    {
        std::cerr << "Error loading texture from SOIL" << std::endl;
        return 1;
    }
    
    GLuint image_uniform = glGetUniformLocation(comp_program, "image");
    
    auto clock = FPSLimiter(1.0 / 30);
    auto measureFPS = Measurer();
    while (!glfwWindowShouldClose(window))
    {
        clock.wait();
        glfwPollEvents();
        std::cout << "FPS: " << 1.0 / measureFPS.get_s() << std::endl;
        measureFPS.update();
        
        update_position(window);
        
        glUseProgram(comp_program);
        
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, tex);
        glUniform1i(image_uniform, 0);
        
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, tex_2d);
        glUniform1i(texture1_uniform, 1);
        
        glUniformMatrix4fv(pos_mat_uniform, 1, GL_FALSE, glm::value_ptr(pos_mat));
        glUniformMatrix4fv(inv_rot_mat_uniform, 1, GL_FALSE, glm::value_ptr(inv_rot_mat));
        
        glBindImageTexture(0, tex, 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA8UI);
        
        glUniform1i(width_uniform, IMG_WIDTH);
        glUniform1i(height_uniform, IMG_HEIGHT);
        
        glDispatchCompute(IMG_WIDTH / WG_WIDTH, IMG_HEIGHT / WG_HEIGHT, 1);
        
        glUseProgram(0);
        
        int width, height;
        glfwGetFramebufferSize(window, &width, &height);
        
        glViewport(0, 0, width, height);
        
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glUseProgram(program);
        
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, tex);
        glUniform1i(sampler_uniform, 0);
        
        glBindVertexArray(vao);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        
        glUseProgram(0);
        
        glfwSwapBuffers(window);
    }
    
    glDeleteProgram(program);
    glfwTerminate();
    
    return 0;
}
