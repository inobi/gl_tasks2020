#version 330
// FRAGMENT SHADER

in vec2 texCoord;
uniform sampler2D texSampler;
out vec4 fragColor;

void main()
{
    fragColor = texture(texSampler, texCoord);
}
