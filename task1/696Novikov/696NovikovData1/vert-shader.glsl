#version 330
// VERTEX SHADER

layout(location = 0) in vec2 vertexPosition;
out vec2 texCoord;

void main()
{
    texCoord = vec2((vertexPosition.x + 1) / 2, (vertexPosition.y + 1) / 2);
    gl_Position = vec4(vertexPosition.x, vertexPosition.y, 0, 1);
}
