#version 430 core

layout(local_size_x = 16, local_size_y = 16, local_size_z = 1) in;
const int N_SPHERES = 3;
const float EPS = 1e-6;
const float PI = 3.141592653589793;

layout(std140, binding=0) uniform Spheres
{
    vec3 center;
    float rad;
    ivec3 color;
} spheres[N_SPHERES];

layout (rgba8ui) uniform uimage2D image;

uniform mat4 position_mat;
uniform mat4 inv_rot_mat;

uniform sampler2D texture1;
uniform int img_width;
uniform int img_height;

vec2 getSphereUV(vec3 coord)
{
    float phi = atan(coord.y, coord.x) / (2 * PI) + 0.5;
    float theta = atan(coord.z / length(coord.xy)) / PI + 0.5;
    return vec2(phi, theta);
}

void main()
{
    //int wg_id = gl_WorkGroupID.z * gl_NumWorkGroups.x * gl_NumWorkGroups.y +
    //            gl_WorkGroupID.y * gl_NumWorkGroups.x + gl_WorkGroupID.x;
    //int id = gl_LocalInvocationIndex
    //    + gl_WorkGroupSize.x * gl_WorkGroupSize.y * gl_WorkGroupsSize.z * wg_id;
    //int row = id / WIDTH;
    //int col = id % WIDTH;
    int row = int(gl_GlobalInvocationID.x);
    int col = int(gl_GlobalInvocationID.y);
    float dw = 2.0 / img_width;
    float dh = 2.0 / img_height;
    float x_base = dw * (row + 0.5) - 1.0;
    float z_base = dh * (col + 0.5) - 1.0;
    vec3 line = normalize(vec3(x_base, 1.0, z_base));
    
    int intersect_sphere = -1;
    float intersect_dist = 0.0;
    for (int i = 0; i < N_SPHERES; ++i)
    {
        vec3 center = (position_mat * vec4(spheres[i].center.xyz, 1.0)).xyz;
        float rad = spheres[i].rad;
        float r = dot(line, center);
        if (r < EPS)
            continue;
        vec3 le = r * line;
        float dist = distance(le, center);
        if (dist > rad - EPS)
            continue;
        float dt = sqrt(rad * rad - dist * dist);
        float d = r - dt;
        if (intersect_sphere == -1 || d < intersect_dist)
        {
            intersect_sphere = i;
            intersect_dist = d;
        }
    }
    
    if (intersect_sphere == -1)
        imageStore(image, ivec2(row, col), uvec4(0, 0, 0, 255));
    else
    {
        vec3 center = (position_mat * vec4(spheres[intersect_sphere].center.xyz, 1.0)).xyz;
        vec3 local_uv = (intersect_dist * line - center) / spheres[intersect_sphere].rad;
        vec2 uv = getSphereUV((inv_rot_mat * vec4(local_uv.xyz, 1.0)).xyz);
        vec4 color = texture(texture1, uv);
        // imageStore(image, ivec2(row, col), uvec4(spheres[intersect_sphere].color, 255));
        imageStore(image, ivec2(row, col), uvec4(color * 255));
    }
}
